function guardarDatosUsuario(){
  var nombre = txtNombre.value;
  var email = txtEmail.value;
  var dni = txtDni.value;
  var usuario = {"nombre": nombre, "email": email, "dni": dni};
  localStorage.setItem("nombre", nombre);
  localStorage.setItem("usuarioLS", JSON.stringify(usuario));

  sessionStorage.setItem("nombre", nombre);
  sessionStorage.setItem("usuarioLS", JSON.stringify(usuario));
}

function recuperarDatosUsuario(){
  var local = localStorage.getItem("nombre");
  var session = sessionStorage.getItem("nombre");
  alert("local: "+ local + " session: "+ session);
  document.getElementById("txtNombre").value = local;

  var usuarioLocal = localStorage.getItem("usuarioLS")

  alert(usuarioLocal);
}
